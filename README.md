# README #

### What is this repository for? ###
* I found a link while researching for Machine Learning, and it's a great resource to get to know the subject of ML and everything about it.
* I felt like sharing the link, for people who might want to search for it, and also made a repo for myself, to remind myself of the link whenever I wanna start getting into ML
* [Here is a Link, Check it out](https://github.com/mrdbourke/machine-learning-roadmap)
* [He has a video walkthrough as well here](https://www.youtube.com/watch?v=pHiMN_gy9mk&feature=youtu.be) - for those who feel adventurous for 2.5hr and wanna learn how to use the roadmap, coz it's sick